import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {

  changeName(event: Event) {
    const element = event.target as HTMLInputElement;
    element.value;
  }
  onScroll(event:Event){
    const element=event.target as HTMLElement;
    console.log(element.scroll)
  }
   form!:FormGroup;
  constructor(
    private formBuilder: FormBuilder

  ) {
    this.buildForm()
  }
  ngOnInit(): void {}
  buildForm() {
    this.form = this.formBuilder.group({
      user: ['', Validators.required],
      password: ['', Validators.required],
    });
  }
  showName() {}
  login() {
    if (this.form.valid) {
      console.log(this.form.value);
    }
  }

  Registrar() {
    console.log('genere un evento a redireccionar a la pagina de usuario');

  }

}
